import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from keras.preprocessing.sequence import TimeseriesGenerator
from keras.optimizers import RMSprop
from keras.callbacks import EarlyStopping
from numpy import concatenate
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.stattools import adfuller
import seaborn as sns

class DataUtil:
    @staticmethod
    def diff(timeseries, d, cols: list=None) -> pd.DataFrame:
        origin_timeseries = timeseries.copy()
        result_timeseries = pd.DataFrame()
        diff_timeseries = []
        
        if cols:
            for col in cols:
                diff_timeseries.append(origin_timeseries[col].diff(d))
        else:
            diff_timeseries.append(origin_timeseries.diff(d))
        
        if cols:
            result_timeseries = origin_timeseries.drop(origin_timeseries.index[:d])
            for i, col in enumerate(cols):
                result_timeseries[col] = diff_timeseries[i]
        else:
            result_timeseries = diff_timeseries[0]
            
        return result_timeseries

    @staticmethod
    def undiff(diff_timeseries, origin_timeseries, d, col=None) -> pd.DataFrame:
        if col:
            result = pd.concat([origin_timeseries[:d], diff_timeseries], ignore_index=True)
        else:
            result = pd.concat([pd.Series(origin_timeseries[:d]), pd.Series(diff_timeseries)], ignore_index=True)
        # print(result)
        for i in range(d, diff_timeseries.shape[0]+d):
            # print(result.loc[i, col], result.loc[i-d, col])
            if col:
                result.loc[i, col] = result.loc[i, col] + result.loc[i-d, col]
            else:
                result.loc[i] = result.loc[i] + result.loc[i-d]
        return result
    
class VisualisationUtil:
    @staticmethod
    def plot_predictions(y, y_hat, from_index=0, to_index=7):
        if isinstance(object, list) == False:
            y = list(y)
            y_hat = list(y_hat)
        plt.plot(y_hat[from_index:to_index], color='orange', marker='.')
        plt.plot(y[from_index:to_index], color='blue', marker='.')
        plt.show()

        # calculate overall RMSE
        rmse = np.sqrt(mean_squared_error(list(y_hat[from_index:to_index]), list(y[from_index:to_index])))
        print('Overall mean RMSE: %.3f' % rmse)

def predict(model, test_data_generator):
    new_predictions = model.predict_generator(test_data_generator).squeeze()
    if new_predictions.shape == ():
        new_predictions = [new_predictions.tolist()]
    return new_predictions

def denormalize_data(data, mean, var):
    return (data * np.sqrt(var)) + mean

def expand_window(data, last_train_index, target_index, model, num_predictions=7, timesteps=2, batch_size=128, epochs=10, for_mlp=False):
    predictions = []
    num_iter = int(np.ceil(data[last_train_index:].shape[0]/num_predictions))
       
    for i in range(num_iter):    
        print('tyden:', i)  
        
        X_train = data[:last_train_index]
        y_train = data[:last_train_index, target_index]
        
        start_test_index = last_train_index - timesteps
        end_test_index = last_train_index + num_predictions
        last_train_index += num_predictions
        
        X_test = data[start_test_index:end_test_index]
        y_test = data[start_test_index:end_test_index, target_index]   
        
        train_data_generator = None
        test_data_generator = None
        if for_mlp:
            X_train, y_train = to_mlp_timeseries(X_train, y_train, target_index=target_index, timesteps=timesteps)
            X_test, y_test = to_mlp_timeseries(X_test, y_test, target_index=target_index, timesteps=timesteps)
        else:
            train_data_generator = TimeseriesGenerator(X_train, y_train, length=timesteps, batch_size=batch_size)
            test_data_generator = TimeseriesGenerator(X_test, y_test, length=timesteps, batch_size=batch_size)

#         print('pred', model.layers[0].get_weights())  
#         print(K.eval(self.model.optimizer.lr))
        if for_mlp:
            history = fit_model(model, epochs, X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test, batch_size=batch_size)
        else:
            history = fit_model(model, epochs, train_data_generator=train_data_generator, test_data_generator=test_data_generator)
#         print('po', model.layers[0].get_weights()) 
        
        if for_mlp:
            new_predictions = get_predictions(model, X_test=X_test).squeeze()
        else:
            new_predictions = get_predictions(model, test_data_generator).squeeze()
        if new_predictions.shape == ():
            new_predictions = [new_predictions.tolist()]
        predictions = np.concatenate((predictions, new_predictions))
        
#         print('-------------')
#         print('tyden:', i+1)
#         print(y_train.tail(7))
#         print(y_test.head(10))
#         print('-------------')

    return predictions
        
def get_predictions(model, test_data_generator=None, X_test=None):   
    if test_data_generator:
        return model.predict_generator(test_data_generator)
    else:
        return model.predict(X_test)
    
def fit_model(model, epochs, train_data_generator=None, test_data_generator=None, X_train=None, y_train=None, X_test=None, y_test=None, batch_size=1):
    if (train_data_generator == None) and (test_data_generator == None):
        return model.fit(X_train, y_train, epochs=epochs, verbose=0, shuffle=False, 
                         batch_size=batch_size,
#                   callbacks=[EarlyStopping(monitor='val_loss', patience=20, verbose=1)]
                        )
    else:    
        return model.fit_generator(train_data_generator, epochs=epochs, verbose=0, shuffle=False, 
                               validation_data=test_data_generator,
                               callbacks=[EarlyStopping(monitor='val_loss', patience=20, verbose=1)],
                                use_multiprocessing=True,
                              workers=4) 

def compute_rmses(y, y_hat, window=7):
    rmses = []
    for y_chunk, y_hat_chunk in zip(list(divide_chunks(y, window)), list(divide_chunks(y_hat, window))):
        rmses.append(np.sqrt(mean_squared_error(y_chunk, y_hat_chunk)))
    return rmses
    
def divide_chunks(data, n): 
    for i in range(0, len(data), n):  
        yield data[i:i + n]     
        
def to_mlp_timeseries(X, y, target_index, timesteps):
    X_result = list()
    for i in range(len(X)-timesteps):
        end_ix = i + timesteps
        X_result.append(X[i:end_ix].flatten())
    return np.array(X_result), y[timesteps:]                    
        
        
# #data.plot(subplots=True, figsize=(15, 15))
# temp_without_season = data['teplota_prumerna'].diff(365).dropna()
# #tsplot(temp_without_season)
# temp_stationary = temp_without_season.diff(1).dropna()
# #tsplot(temp_stationary)

# def test_stationarity(timeseries):
#     rolmean = timeseries.rolling(window=30).mean()
#     rolstd = timeseries.rolling(window=30).std()
    
#     plt.figure(figsize=(14,5))
#     sns.despine(left=True)
#     orig = plt.plot(timeseries, color='blue',label='Original')
#     mean = plt.plot(rolmean, color='red', label='Rolling Mean')
#     std = plt.plot(rolstd, color='black', label = 'Rolling Std')

#     plt.legend(loc='best'); plt.title('Rolling Mean & Standard Deviation')
#     plt.show()
    
#     print ('<Results of Dickey-Fuller Test>')
#     dftest = adfuller(timeseries, autolag='AIC', regression='ctt')
#     dfoutput = pd.Series(dftest[0:4],
#                          index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
#     for key,value in dftest[4].items():
#         dfoutput['Critical Value (%s)'%key] = value
#     print(dfoutput)        
    
    
# def tsplot(y, lags=15):    
#     # layout
#     fig = plt.figure(figsize=(14, 6))
#     layout = (2, 2)
#     ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
#     acf_ax = plt.subplot2grid(layout, (1, 0))
#     pacf_ax = plt.subplot2grid(layout, (1, 1))
    
#     # ts plot
#     y.plot(ax=ts_ax)
#     ts_ax.set_title('Time series');
    
#     # acf, pacf
#     plot_acf(y, lags=lags, ax=acf_ax, alpha=0.5)
#     plot_pacf(y, lags=lags, ax=pacf_ax, alpha=0.5)     